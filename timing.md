# Best method to simulate ode evolution _in silico_

When one wants to optimize a model involving ODEs, a question comes rapidly to mind: the time efficiency of our code.

There exists a range of choices one can make regarding the programming language to use. Python is a high level programming language which makes it relatively slow for an optimization problem (as compared to C for example). However sometime python can save a lot time in the programming phase as it takes fewer lines to write the same program in python than in C. The economy made during the programming phase may sometime balance the running efficiency.

Luckily there exists a compromize that  keeps the python easiness but improve the code efficiency. The most brutal is to wrap the simulation part coded in C with python code. This way the python code is used to compile and run the C code at each generation. If the compilation time and catching the standard output fom the run is efficient enough it can be an advantageous solution. A more native way python 
can communicate with the C language is either _python.h_ class in C or C++ to create a compiled python class or using the _cython_ class that will use a python code to generate a compiled C code that can be used as a class in another python code. The objective of this article is produce a banchmark for the the following methods:

1. Running the simulations with everything written in python
2. Running the simulations using the scipy tools
3. Running the simulations by compiling and running the integrator and ODE written in C
4. Running the simulations with the ODE and integrator written in cython
5. Running the simulations by interfacing the ODE and integrator written with python using the python.h C class.

Finally, I want to test the last methods against another trial which uses a new programming language: _julia_. It is known as being a a relativelly high level language that shows performences close to those of C. So I add a other test to the list:

6. Running the simulations with evrything written in julia


## Reading parameters from file

Before even starting the test, I wanted to check the time the program needs to read files (To load the parameters). I wrote two bits  of code, one in C and one in python. The code is present in the source file but only the piece of code concerned with the reading of the parameters is timed: 

- In C

``` c

t0 = clock();
int i;
int NRUN;
double dt,tMax,g,h,n,l;
read_parameters(&NRUN,&dt,&tMax,&g,&h,&n,&l);  
for(i=0;i<(NRUN-1);i++){
	read_parameters(&NRUN,&dt,&tMax,&g,&h,&n,&l);
	}
t1 = clock();
```

- In python

``` python

t0 = time.time()
param = read_parameters("PARAMETERS_GENE")   
for i in range(param["NRUN"]-1):
	param = read_parameters("PARAMETERS_GENE")
t1 = time.time()    
```

The C program was 10 times faster to read the "PARAMETER_GENE" file than the python program was ($4.14\times10^{-6}$ and $5.4\times10^{-5}$).

Note that I tried two ways of repeating the trials in python: using a for loop and using the timeit function. Both methods resulted in the same times (difference on the third significative number). 


## 
