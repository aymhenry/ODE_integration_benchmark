#include <stdlib.h>
#include <time.h>
#include <math.h>
#define N 3


void read_parameters(int *NRUN,double *dt,double *tMax, double *g, double *h, double *n, double *l){
  FILE *fp;
  char line[20];
  fp = fopen("PARAMETERS_GENE","r"); // read mode
   if( fp == NULL )
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }      
   fscanf(fp, "%s = %d;", line, NRUN);
   fscanf(fp, "%s = %lf;", line, dt);   
   fscanf(fp, "%s = %lf;", line, tMax);
   fscanf(fp, "%s = %lf;", line, g);
   fscanf(fp, "%s = %lf;", line, h);
   fscanf(fp, "%s = %lf;", line, n);
   fscanf(fp, "%s = %lf;", line, l);
   fclose(fp);
}

void derivative_fct(double t,double *x,double *dx,double *p){
  //printf("[%f,%f,%f]\n",x[0],x[1],x[2]);
  dx[0] = p[0]*(x[0]-x[1]);
  dx[1] = x[0]*(p[1] - x[2]) - x[1];
  dx[2] = x[0]*x[1] - p[2]*x[2];
}

double hill(double x,double h,double n){
  return 1/(1 + pow((x/h),n));
}

double derivatives_gene(double t,double *x,double *dx,double *param){
  dx[0] = param[0]*hill(x[1],param[1],param[2]) -param[3]*x[0];
  dx[1] = param[0]*hill(x[2],param[1],param[2]) -param[3]*x[1];
  dx[2] = param[0]*hill(x[0],param[1],param[2]) -param[3]*x[2];
}





double * integrate (void (*fct)(double t,double x[],double *dx,double *p) ,double dt,double tmax,double *x,double *param){
  
  double t =0;
  double dx[N];
  int i;
  double k1[N];
  double k2[N];
  double k3[N];
  double k4[N];

  double u2[N];
  double u3[N];
  double u4[N];      
  
  while(t<tmax){

    //
    fct(t,x,k1,param);
    
    for(i=0;i<N;i++) u2[i] = x[i] + dt/2*k1[i];
    fct(t+dt,u2,k2,param);

    for(i=0;i<N;i++) u3[i] = x[i] + dt/2*k2[i];
    fct(t+dt/2,u3,k3,param);

    for(i=0;i<N;i++) u4[i] = x[i] + dt*k3[i];
    fct(t+dt,u4,k4,param);

    for(i=0;i<N;i++) x[i] += k1[i]/6 + k2[i]/3 + k3[i]/3 +k4[i]/6;
    
    t += dt;
  }  

}

void run_evolution_C(void){
  clock_t t0,t1;
  int i,j;
  t0 = clock();
  int NRUN;
  double dt,tMax,g,h,n,l;
  read_parameters(&NRUN,&dt,&tMax,&g,&h,&n,&l);
  double param [] = {g,h,n,l};
  double x[]={0,0,0};
  for(i=0;i<NRUN;i++){
    for (j=0;j<N;j++) x[j] = 0;
    x[0] = 0.1;
    integrate(derivative_fct,dt,tMax,x,param);
  }
  t1 = clock();
  printf("C code:%e sec/run\n", difftime(t1,t0)/(CLOCKS_PER_SEC *(double)NRUN));
}
