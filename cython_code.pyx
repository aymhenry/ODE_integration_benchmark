from libc.time cimport clock,clock_t,CLOCKS_PER_SEC	  

def hill(x,h,n):
    return 1/(1 + (x/h)**n)
    
def derivatives_gene(t,x,param):
    dx = [0,0,0]
    dx[0] = param["g"]*hill(x[1],param["h"],param["n"]) -param["l"]*x[0]
    dx[1] = param["g"]*hill(x[2],param["h"],param["n"]) -param["l"]*x[1]
    dx[2] = param["g"]*hill(x[0],param["h"],param["n"]) -param["l"]*x[2]
    return dx

def integrate_RK4(fct,dt,tMax,x,param):
    t = 0
    N = len(x)
    k1 = [0,0,0]
    k2 = [0,0,0]
    k3 = [0,0,0]
    k4 = [0,0,0]
    res = [x]
    while t<tMax:
       k1 = fct(t,x,param)
       k2 = fct(t+dt/2,[x[i]+dt/2*k1[i] for i in range(N)],param)
       k3 = fct(t+dt/2,[x[i]+dt/2*k2[i] for i in range(N)],param)
       k4 = fct(t+dt,[x[i]+dt*k3[i] for i in range(N)],param)       
       x = [x[i] + dt/6*(k1[i] + 2*k2[i] + 2*k3[i] +k4[i]/6) for i in range(N)]
       res.append(x)
       t+=dt
    return res

def read_parameters(file_name):
    param = {}
    with open("PARAMETERS_GENE") as param_file:
        for lines in param_file:
            lines = lines.split(" = ")
            try:
                param[lines[0]] = int(lines[1].rstrip(";\n"))
            except ValueError:
                param[lines[0]] = float(lines[1].rstrip(";\n"))
    return param
    
def run_evolution_cython():
    param = read_parameters("PARAMETERS_GENE")
    cdef clock_t t1 = clock()
    for i in range(param["NRUN"]):        
        x0 = [0.1,0,0]
        res = integrate_RK4(derivatives_gene,param["dt"],param["tMax"],x0,param)

    cdef clock_t t2 = clock()

    print("Cython:%e sec/run"%((t2-t1)/(CLOCKS_PER_SEC*param["NRUN"])))
