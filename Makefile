default: cython_code.cpython-35m-x86_64-linux-gnu.so run_C
	@echo "Done"
	./run_C
cython_code.cpython-35m-x86_64-linux-gnu.so: cython_code.pyx
	python3 setup_cython.py build_ext --inplace


run_C: functions.h C_code.c
	gcc -o run_C C_code.c -lm
	chmod +x run_C

clean:
	rm cython_code.c cython_code.cpython-35m-x86_64-linux-gnu.so run_C
run: run_C cython_code.cpython-35m-x86_64-linux-gnu.so
	python3 run_script.py
