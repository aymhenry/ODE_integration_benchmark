import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.integrate import ode
import time
import timeit

def hill(x,h,n):
    return 1/(1 + (x/h)**n)

def derivatives_gene(t,x,param):
    dx = [0,0,0]
    dx[0] = param["g"]*hill(x[1],param["h"],param["n"]) -param["l"]*x[0]
    dx[1] = param["g"]*hill(x[2],param["h"],param["n"]) -param["l"]*x[1]
    dx[2] = param["g"]*hill(x[0],param["h"],param["n"]) -param["l"]*x[2]
    return dx


def integrate_RK4(fct,dt,tMax,x,param):
    t = 0
    N = len(x)
    k1 = [0,0,0]
    k2 = [0,0,0]
    k3 = [0,0,0]
    k4 = [0,0,0]
    res = [x]
    while t<tMax:
       k1 = fct(t,x,param)
       k2 = fct(t+dt/2,[x[i]+dt/2*k1[i] for i in range(N)],param)
       k3 = fct(t+dt/2,[x[i]+dt/2*k2[i] for i in range(N)],param)
       k4 = fct(t+dt,[x[i]+dt*k3[i] for i in range(N)],param)       
       x = [x[i] + dt/6*(k1[i] + 2*k2[i] + 2*k3[i] +k4[i]/6) for i in range(N)]
       res.append(x)
       t+=dt
    return res


def integrate_euler(fct,dt,tMax,x,param):
    t = 0
    N = len(x)
    res = [x]
    tt = [0]
    
    while t<tMax:
       dx = fct(t,x,param)
       print(dx)
       x = [x[i]+dx[i]*dt for i in range(N)]
       res.append(x)
       t+=dt
       tt.append(t)
    return(tt,res)

def integrate_scipy(fct,dt,tMax,x,param):
    r = ode(fct)
    r.set_integrator('dopri5',rtol=1e-5,nsteps=1000)
    r.set_f_params(param)
    r.set_initial_value(x,0)
    res = [x]
    while r.t<tMax:
        r.integrate(r.t+dt)
        res.append(r.y)
    return(res)


def read_parameters(file_name):
    param = {}
    with open("PARAMETERS_GENE") as param_file:
        for lines in param_file:
            lines = lines.split(" = ")
            try:
                param[lines[0]] = int(lines[1].rstrip(";\n"))
            except ValueError:
                param[lines[0]] = float(lines[1].rstrip(";\n"))
    return param

                
    
def run_evolution_python_alone():
    param = read_parameters("PARAMETERS_GENE")
    t1 = time.time()
    for i in range(param["NRUN"]):        
        x0 = [0.1,0,0]
        res = integrate_RK4(derivatives_gene,param["dt"],param["tMax"],x0,param)
        t2 = time.time()
    t2 = time.time()

    print("Python alone:%e sec/run"%((t2-t1)/param["NRUN"]))

def run_evolution_scipy():
    param = read_parameters("PARAMETERS_GENE")
    times = []
    t1 = time.time()
    for i in range(param["NRUN"]):        
        x0 = [0.1,0,0]
        res = integrate_scipy(derivatives_gene,param["dt"],param["tMax"],x0,param)
    t2 = time.time()        
    print("Python Scipy:%e sec/run"%((t2-t1)/param["NRUN"]))
    return(times)

def test_file_opening():
    N = 10000
    t0 = time.time()
    param = read_parameters("PARAMETERS_GENE")   
    for i in range(param["NRUN"]-1):
        param = read_parameters("PARAMETERS_GENE")
    t1 = time.time()
    
    t2 = timeit.timeit(lambda : read_parameters("PARAMETERS_GENE"),number=param["NRUN"])
    print("NRUN:%d"%param["NRUN"])
    print("execution time(for loop)\t:%g"%((t1-t0)/param["NRUN"]))
    print("execution time(timeit)\t:%g"%(t2/param["NRUN"]))    

    ## Plot Histogram

    # fig, ax = plt.subplots()
    # n, bins, patches = ax.hist(times, 50)
    
    
    # ax.set_xlabel('Smarts')
    # ax.set_ylabel('Probability density')
    # ax.set_title(r'Histogram of IQ: $\mu=100$, $\sigma=15$')
    
    # # Tweak spacing to prevent clipping of ylabel
    # fig.tight_layout()
    # plt.show()



    
if __name__ == "__main__":
    res = run_evolution()
