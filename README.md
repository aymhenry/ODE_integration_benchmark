# Naive comparison between the C, python, and Julia performance to integrate ODEs

This runs a simple comparison of integration performance between different tools:

- python3
- julia
- cython
- C binding in python (_python.h_)
- pure C

Some files need to be compiled, in bash run:

``` bash
    make
```

To launch the tests, simply run

```bash
   python3 run_script.py		
```